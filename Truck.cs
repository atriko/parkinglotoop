﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Truck : Vehicle // Truck class extending from abstract class vehicle
{
    public Truck()
    {
        size = VehicleSize.Large;
        spotsNeeded = 3;
        GenerateLicensePlate();
    }
    
}
