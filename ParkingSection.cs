﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkingSection : MonoBehaviour // Parking section class for different sections in the lot
{
    public char parkingSection; // Parking section character
    public List<ParkingSpot> parkingSpots = new List<ParkingSpot>(); //Parking spots in one section
}
