﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Vehicle : MonoBehaviour // Vehicle class its abstract because we cant have a object from vehicle. Vehicles should instantiate from sub-classes.
{
    public enum VehicleSize 
    {
        Bicycle,
        Standart,
        Large
    }

    public VehicleSize size; // All vehicles should have a size 
    public int spotsNeeded; // Vehicles can have different spots needed
    public string licensePlate; // All vehicles have a unique license plate

    public List<ParkingSpot> occupyingSpots = new List<ParkingSpot>(); // Occupied spots by vehicle
    
    protected static int licensePlateLength = 6; // License plate length default is 6
    protected static List<string> licensePlates = new List<string>(); // All license plates of vehicles

    protected void GenerateLicensePlate()
    {
        const string plateChars = "QWERTYUIOPASDFGHJKLZXCVBNM";
        for (int i = 0; i < licensePlateLength; i++)
        {
            licensePlate += plateChars[UnityEngine.Random.Range(0, plateChars.Length)]; // Generate random plate
        }
        if (!licensePlates.Contains(licensePlate)) // if license plate is not used we will take it if not we will recall generate
        {
            licensePlates.Add(licensePlate);
        }
        else
        {
            GenerateLicensePlate();
        }
    }

    protected void ParkToSpot(ParkingSpot spot)
    {
        //Check if the spot is available
        if (!spot.isOccupied)
        {
            //Check if you can fit the spot
            if (spotsNeeded < 2) // if spots needed is less than 2 and its already not occupied so we can park instantly
            {
                spot.Park(this);
                occupyingSpots.Add(spot);
            }
            else
            {
                int freeSpots = 0;
                for (int i = 0; i < spotsNeeded; i++) // we will check the neighbours of the spot we try to park to see if we can fit
                {

                    // IMPORTANT ! -- This part needs a null check for not exceeding spot numbers --

                    if (spot.parkingSpots[spot.spotNumber + i].isOccupied == false) // look to one direction to check spots
                    {
                        freeSpots++;
                    }
                    else // if one of them is occupied we reset
                    {
                        freeSpots = 0;
                    }
                }
                if (spotsNeeded == freeSpots) // if we have find enough spots we will park on each spot
                {
                    for (int i = 0; i < spotsNeeded; i++)
                    {
                        spot.parkingSpots[spot.spotNumber + i].Park(this);
                    }
                }
                else
                {
                    for (int i = 0; i < spotsNeeded; i++) // now we will check the other direction -notice the minus i in if statement-
                    {
                        // IMPORTANT ! -- This part needs a null check for not exceeding spot numbers --
                        if (spot.parkingSpots[spot.spotNumber - i].isOccupied == false)
                        {
                            freeSpots++;
                        }
                        else
                        {
                            freeSpots = 0;
                        }
                    }
                    if (spotsNeeded == freeSpots)
                    {
                        for (int i = 0; i < spotsNeeded; i++)
                        {
                            spot.parkingSpots[spot.spotNumber - i].Park(this);
                        }
                    }
                    else // if we cant fit to the spots
                    {
                        Debug.Log("Sorry your vehicle can't fit here");
                    }
                }
            }
        }
        else // if the spot we choose at first is occupied
        {
            Debug.Log("Sorry this spot is already taken");
        }
    }
    
    protected void LeaveSpot()
    {
        foreach (var spot in occupyingSpots) // for every spot we occupy we unpark and remove from them from occupying spots
        {
            spot.Unpark();
            occupyingSpots.Remove(spot);
        }
    }
}
