﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkingSpot : ParkingSection // Parking spot class extending from sections
{
    //Has a spot number
    public int spotNumber;

    //Has a spot section
    public ParkingSection section;

    //Has a bool for occupation
    public bool isOccupied;

    //Has a vehicle if occupied
    public Vehicle vehicle;

    public ParkingSpot()
    {
        spotNumber = parkingSpots.Count + 1;
        isOccupied = false;
        parkingSpots.Add(this);
    }
    public void Park(Vehicle vehicle)
    {
        isOccupied = true;
        this.vehicle = vehicle;
    }
    public void Unpark()
    {
        isOccupied = false;
        vehicle = null;
    }
}
