﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bicycle : Vehicle // Bicycle class extending from abstract class vehicle
{
    public Bicycle()
    {
        size = VehicleSize.Bicycle;
        spotsNeeded = 1;
        GenerateLicensePlate();
    }
}
