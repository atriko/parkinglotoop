﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : Vehicle // Car class extending from abstract class vehicle
{
    public Car()
    {
        size = VehicleSize.Standart;
        spotsNeeded = 1;
        GenerateLicensePlate();
    }
}
